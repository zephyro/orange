<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="https://unpkg.com/swiper/css/swiper.min.css">
        <link rel="stylesheet" href="css/main.css">

        <style>

        </style>

    </head>
    <body>


        <div class="page">

            <div class="page__wrap">

                <header class="header">
                    <div class="container">
                        <div class="header__row">
                            <a href="#" class="header__toggle nav_toggle">
                                <span></span>
                            </a>
                            <a class="header__phone" href="tel:+74996648560">+7 (499) 664-85-60</a>
                            <nav class="header__nav">
                                <ul>
                                    <li><a href="#">ПРОДУКЦИЯ</a></li>
                                    <li><a href="#">УСЛУГИ</a></li>
                                    <li><a href="#">ИНФОРМАЦИЯ</a></li>
                                    <li><a href="#">ПРАЙС</a></li>
                                    <li><a href="#">НОВОСТИ</a></li>
                                    <li><a href="#">КОНТАКТЫ</a></li>
                                </ul>
                            </nav>
                            <div class="header__nav_layout nav_toggle"></div>
                            <div class="header__links">
                                <a href="#">
                                    <i>
                                        <svg class="ico_svg" viewBox="0 0 20 20"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__search" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </i>
                                </a>
                                <a href="#">
                                    <i>
                                        <svg class="ico_svg" viewBox="0 0 24 24"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__telegram" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </i>
                                </a>
                            </div>
                        </div>
                        <a href="/" class="header__logo">
                            <img src="img/logo.png" class="img_fluid" alt="">
                        </a>
                    </div>
                </header>

                <section class="goods">
                    <div class="container">
                        <div class="goods__row">
                            <div class="goods__col">
                                <a href="#" class="goods__item">
                                    <div class="goods__item_image">
                                        <img src="img/image__wood.jpg" class="img_fluid" alt="">
                                    </div>
                                    <div class="goods__item_text"><span>Огне</span>защита древесины, материалов и конструкций на ее основе</div>
                                </a>
                            </div>
                            <div class="goods__col">
                                <a href="#" class="goods__item">
                                    <div class="goods__item_image">
                                        <img src="img/image__metal.jpg" class="img_fluid" alt="">
                                    </div>
                                    <div class="goods__item_text"><span>Огне</span>защита металлических конструкций, воздуховодов</div>
                                </a>
                            </div>
                            <div class="goods__col">
                                <a href="#" class="goods__item">
                                    <div class="goods__item_image">
                                        <img src="img/image__kabel.jpg" class="img_fluid" alt="">
                                    </div>
                                    <div class="goods__item_text"><span>Огне</span>защита кабельной продукции и проходок</div>
                                </a>
                            </div>
                            <div class="goods__col">
                                <a href="#" class="goods__item">
                                    <div class="goods__item_image">
                                        <img src="img/image__beton.jpg" class="img_fluid" alt="">
                                    </div>
                                    <div class="goods__item_text"><span>Огне</span>защита бетонных и железобетонных конструкций</div>
                                </a>
                            </div>
                            <div class="goods__col">
                                <a href="#" class="goods__item">
                                    <div class="goods__item_image">
                                        <img src="img/image__polymer.jpg" class="img_fluid" alt="">
                                    </div>
                                    <div class="goods__item_text"><span>Огне</span>защита полимерных строительных материалов</div>
                                </a>
                            </div>
                            <div class="goods__col">
                                <a href="#" class="goods__item">
                                    <div class="goods__item_image">
                                        <img src="img/image__textile.jpg" class="img_fluid" alt="">
                                    </div>
                                    <div class="goods__item_text"><span>Огне</span>защита тканей и ковровых покрытий</div>
                                </a>
                            </div>
                        </div>
                    </div>
                </section>

                <section class="info">
                    <div class="container">
                        <div class="info__wrap">
                            <div class="info__content">
                                <div class="info__title"><span class="color_red">Огне</span>защитные составы <br/><span class="text_uppercase">НЕГОРЫНЫЧ</span></div>
                                <p>Для получения надежного огнезащитного покрытия термостойкие составы послойно наносятся на несущие конструкции, балки и прочие элементы из металла при выполнении внутренних и наружных работ.</p>
                                <p>Компания является производителем конструктивной огнезащиты и проводит лицензированные работы по огнезащите металлических элементов на промышленных и гражданских объектах Москвы и регионов России.</p>
                            </div>
                            <div class="info__media">
                                <div class="info__image">
                                    <img src="img/info_image.jpg" class="img_fluid" alt="">
                                    <div class="info__pattern info__pattern_one">
                                        <svg class="ico_svg" viewBox="0 0 139 59"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#info__pattern_one" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </div>
                                    <div class="info__pattern info__pattern_two">
                                        <svg class="ico_svg" viewBox="0 0 99 42"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#info__pattern_two" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </div>
                                    <div class="info__pattern info__pattern_three">
                                        <svg class="ico_svg" viewBox="0 0 132 35"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#info__pattern_three" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <section class="advantage">
                    <div class="container">
                        <div class="advantage__header">НЕГОРЫНЫЧ: преимущества</div>
                        <div class="advantage__wrap">
                            <div class="advantage__slider swiper-container">
                                <div class="swiper-wrapper">

                                    <div class="swiper-slide">
                                        <div class="advantage__item">
                                            <a href="#" class="advantage__title">Заголовок статьи <br/>Тонкослойная огнезащита</a>
                                            <a href="#" class="advantage__tag advantage__tag_green">Категория: дерево</a>
                                            <img src="img/advantage__image.jpg" class="img_fluid" alt="">
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="advantage__item">
                                            <a href="#" class="advantage__title">Заголовок статьи <br/>Тонкослойная огнезащита</a>
                                            <a href="#" class="advantage__tag advantage__tag_blue">Категория: кабели</a>
                                            <img src="img/advantage__image.jpg" class="img_fluid" alt="">
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="advantage__item">
                                            <a href="#" class="advantage__title">Конструктивная огнезащита металлоконструкций</a>
                                            <a href="#" class="advantage__tag advantage__tag_black">Категория: металл</a>
                                            <img src="img/advantage__image.jpg" class="img_fluid" alt="">
                                        </div>
                                    </div>

                                    <div class="swiper-slide">
                                        <div class="advantage__item">
                                            <a href="#" class="advantage__title">Заголовок статьи <br/>Тонкослойная огнезащита</a>
                                            <a href="#" class="advantage__tag advantage__tag_green">Категория: дерево</a>
                                            <img src="img/advantage__image.jpg" class="img_fluid" alt="">
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="advantage__item">
                                            <a href="#" class="advantage__title">Заголовок статьи <br/>Тонкослойная огнезащита</a>
                                            <a href="#" class="advantage__tag advantage__tag_blue">Категория: кабели</a>
                                            <img src="img/advantage__image.jpg" class="img_fluid" alt="">
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="advantage__item">
                                            <a href="#" class="advantage__title">Конструктивная огнезащита металлоконструкций</a>
                                            <a href="#" class="advantage__tag advantage__tag_black">Категория: металл</a>
                                            <img src="img/advantage__image.jpg" class="img_fluid" alt="">
                                        </div>
                                    </div>

                                    <div class="swiper-slide">
                                        <div class="advantage__item">
                                            <a href="#" class="advantage__title">Заголовок статьи <br/>Тонкослойная огнезащита</a>
                                            <a href="#" class="advantage__tag advantage__tag_green">Категория: дерево</a>
                                            <img src="img/advantage__image.jpg" class="img_fluid" alt="">
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="advantage__item">
                                            <a href="#" class="advantage__title">Заголовок статьи <br/>Тонкослойная огнезащита</a>
                                            <a href="#" class="advantage__tag advantage__tag_blue">Категория: кабели</a>
                                            <img src="img/advantage__image.jpg" class="img_fluid" alt="">
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="advantage__item">
                                            <a href="#" class="advantage__title">Конструктивная огнезащита металлоконструкций</a>
                                            <a href="#" class="advantage__tag advantage__tag_black">Категория: металл</a>
                                            <img src="img/advantage__image.jpg" class="img_fluid" alt="">
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <button class="advantage__nav advantage__nav_prev">
                                <svg class="ico_svg" viewBox="0 0 50 78"  xmlns="http://www.w3.org/2000/svg">
                                    <use xlink:href="img/sprite_icons.svg#icon__angle_left" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                </svg>
                            </button>
                            <button class="advantage__nav advantage__nav_next">
                                <svg class="ico_svg" viewBox="0 0 50 78"  xmlns="http://www.w3.org/2000/svg">
                                    <use xlink:href="img/sprite_icons.svg#icon__angle_right" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                </svg>
                            </button>
                        </div>
                    </div>
                </section>

                <footer class="footer">
                    <div class="container">
                        <a href="/" class="footer__logo">
                            <img src="img/logo.png" class="img_fluid" alt="">
                        </a>
                        <div class="footer__row">
                            <div class="footer__phone">
                                <a href="tel:+74996648560">+7 (499) 664-85-60</a>
                            </div>
                            <nav class="footer__nav">
                                <div class="footer__nav_group">
                                    <ul>
                                        <li><a href="#">ПРОДУКЦИЯ</a></li>
                                        <li><a href="#">УСЛУГИ</a></li>
                                    </ul>
                                    <ul>
                                        <li><a href="#">ИНФОРМАЦИЯ</a></li>
                                        <li><a href="#">НОВОСТИ</a></li>
                                    </ul>
                                </div>
                                <div class="footer__nav_group">
                                    <ul>
                                        <li><a href="#">ПРАЙС</a></li>
                                        <li><a href="#">КОНТАКТЫ</a></li>
                                    </ul>
                                    <ul>
                                        <li><a href="#">ПОИСК</a></li>
                                        <li><a href="#">КАРТА САЙТА</a></li>
                                    </ul>
                                </div>
                            </nav>
                            <ul class="footer__links">
                                <li><a href="#">Политика конфиденциальности</a></li>
                                <li><a href="#">Условия использования сайта</a></li>
                                <li><a href="#">Разработка сайта</a></li>
                            </ul>
                        </div>
                    </div>
                </footer>

            </div>

        </div>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-3.4.1.min.js"><\/script>')</script>
        <script src="https://unpkg.com/swiper/js/swiper.min.js"></script>
        <script src="js/vendor/svg4everybody.legacy.min.js"></script>
        <script src="js/main.js"></script>

    </body>
</html>
