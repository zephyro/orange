// SVG IE11 support
svg4everybody();



(function() {

    $('.nav_toggle').on('click', function(e){
        e.preventDefault();
        $('body').toggleClass('nav_open');
    });

}());

// Slider
var advantage = new Swiper ('.advantage__slider', {
    loop: true,
    slidesPerView: 1,
    spaceBetween: 22,
    navigation: {
        nextEl: '.advantage__nav_next',
        prevEl: '.advantage__nav_prev',
    },
    breakpoints: {
        768: {
            slidesPerView: 1,
            spaceBetween: 14
        },
        1024: {
            slidesPerView: 3,
            spaceBetween: 14
        },
        1200: {
            slidesPerView: 3,
            spaceBetween: 17
        }
    }
});